Test that IndexedDB objects that have been deleted throw exceptions

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


indexedDB = self.indexedDB || self.webkitIndexedDB || self.mozIndexedDB || self.msIndexedDB || self.OIndexedDB;

dbname = "deleted-objects.html"
indexedDB.deleteDatabase(dbname)
indexedDB.open(dbname)

testStore():
deletedStore = connection.createObjectStore('deletedStore')
connection.deleteObjectStore('deletedStore')

Expecting exception from deletedStore.put(0, 0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.add(0, 0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.delete(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.delete(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.get(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.get(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.clear()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.openCursor()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.openCursor(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.openCursor(0, 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.openCursor(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.openCursor(IDBKeyRange.only(0), 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.createIndex('name', 'path')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.index('name')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.deleteIndex('name')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.count()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.count(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedStore.count(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'

testIndex():
store = connection.createObjectStore('store')
deletedIndex = store.createIndex('deletedIndex', 'path')
store.deleteIndex('deletedIndex')

Expecting exception from deletedIndex.openCursor()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openCursor(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openCursor(0, 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openCursor(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openCursor(IDBKeyRange.only(0), 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openKeyCursor()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openKeyCursor(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openKeyCursor(0, 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openKeyCursor(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.openKeyCursor(IDBKeyRange.only(0), 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.get(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.get(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.getKey(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.getKey(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.count()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.count(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from deletedIndex.count(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'

testTransitiveDeletion():
deletedStore = connection.createObjectStore('deletedStore')
indexOfDeletedStore = deletedStore.createIndex('index', 'path')
connection.deleteObjectStore('deletedStore')

Expecting exception from indexOfDeletedStore.openCursor()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openCursor(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openCursor(0, 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openCursor(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openCursor(IDBKeyRange.only(0), 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openKeyCursor()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openKeyCursor(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openKeyCursor(0, 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openKeyCursor(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.openKeyCursor(IDBKeyRange.only(0), 'next')
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.get(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.get(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.getKey(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.getKey(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.count()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.count(0)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from indexOfDeletedStore.count(IDBKeyRange.only(0))
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'

testObjectStoreCursor():
deletedStore = connection.createObjectStore('deletedStore')
deletedStore.put(0, 0)
deletedStore.openCursor()
cursor = request.result
PASS cursor.key is 0
PASS cursor.value is 0
connection.deleteObjectStore('deletedStore')
Expecting exception from cursor.delete()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.update(1)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.continue()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.advance(1)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'

testIndexCursor():
store.put({id: 123}, 0)
deletedIndex = store.createIndex('deletedIndex', 'id')
deletedIndex.openCursor()
cursor = request.result
PASS cursor.key is 123
PASS cursor.primaryKey is 0
store.deleteIndex('deletedIndex')
Expecting exception from cursor.delete()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.update(1)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.continue()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.advance(1)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'

testIndexOfDeletedStoreCursor():
deletedStore = connection.createObjectStore('deletedStore')
deletedStore.put({id: 123}, 0)
index = deletedStore.createIndex('index', 'id')
index.openCursor()
cursor = request.result
PASS cursor.key is 123
PASS cursor.primaryKey is 0
connection.deleteObjectStore('deletedStore')
Expecting exception from cursor.delete()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.update(1)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.continue()
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
Expecting exception from cursor.advance(1)
PASS Exception was thrown.
PASS code is DOMException.INVALID_STATE_ERR
PASS ename is 'InvalidStateError'
PASS successfullyParsed is true

TEST COMPLETE

