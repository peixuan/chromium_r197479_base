// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto2";

option optimize_for = LITE_RUNTIME;

package power_manager;

// Power supply status sent from powerd to Chrome.
message PowerSupplyProperties {
  enum BatteryState {
    // Line power is connected and the battery is full or being charged.
    CHARGING = 0;

    // Line power is disconnected.
    DISCHARGING = 1;

    // Line power is connected and the battery isn't full, but it also
    // isn't charging (or discharging).  This can occur if the EC believes
    // that the battery isn't authentic and chooses to run directly off
    // line power.
    NEITHER_CHARGING_NOR_DISCHARGING = 2;

    // A USB power source (SDP, DCP, CDP, or ACA) is connected.  The
    // battery may be charging or discharging depending on the negotiated
    // current.
    CONNECTED_TO_USB = 3;

    // Next value to use: 4
  }

  // Does the system have a battery?
  optional bool battery_is_present = 8;

  // Current state of the battery.
  optional BatteryState battery_state = 13;

  // Estimated battery charge as a percent of its total capacity, in the
  // range [0.0, 100.0].
  optional double battery_percent = 7;

  // Estimated time until the battery is empty, in seconds, or zero if the
  // battery isn't discharging.
  optional int64 battery_time_to_empty_sec = 5;

  // Estimated time until the battery is full, in seconds, or zero if the
  // battery isn't charging.
  optional int64 battery_time_to_full_sec = 6;

  // True when |battery_time_to_*| can't be trusted, e.g. because the power
  // source just changed.
  optional bool is_calculating_battery_time = 12 [default = false];

  // Next ID to use: 14
}
